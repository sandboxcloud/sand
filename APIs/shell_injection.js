var log = require('./log/log');
var data = {"name":"shell", "date":""};

module.exports = (string) => {
 var v1 = string.indexOf('>');
 var v2 = string.indexOf('|');
 var v3 = string.indexOf('||');
 var v4 = string.indexOf('&');
 var v5 = string.indexOf('&&');
 var v6 = string.indexOf('sed');
 var v7 = string.indexOf(';');
 var v8 = string.indexOf('rm');
 var v9 = string.indexOf('unset');

 if(v1&&v2&&v3&&v4&&v5&&v6&&v7&&v8&&v9 === -1){
  data.date = new Date();
  log(data);
  return false;
 }
 else return false;
}

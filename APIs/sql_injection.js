//SQL-Injection 공격은 DB를 노리는 공격입니다.
//대부분은 로그인창에서 이루어지기 때문에 id, pw의 값을 받습니다.
//express 서버와 .jade를 사용하는 환경에서 이루어집니다
//id와 pw에서 이루어지는 공격을 감지하고 메세지와 로그를 남깁니다.

var log=require('./log/log');
var data={"name": "sql-injection", "date": ""};

module.exports=(string) => { 
//공격쿼리에서 가장 빈번히 사용되는 문자들입니다
 var v1=string.indexOf('"');
 var v2=string.indexOf(';');
 var v3=string.indexOf('<');
 var v4=string.indexOf('>');
 var v5=string.indexOf('?');
 var v6=string.indexOf('*');


 return function(req, res) {
    var str=NULL;
    //id, pw에 해당 공격쿼리의 키워드가 들어갔는지를 확인하기 위한 코드입니다.
    for (var i in string) {
	if (v1&&v2&&v3&&v4&&v5&&v6 === -1) {
	   data.date=new Date();
	   log(data);
	   res.send(200, {});
	   return false;
	}

	if(i==" ") { //쿼리문들을 필터링하도록 하는 코드입니다.
	   if((str=="select")||(str=="from")||(str=="where")||(str=="insert")||(str=="update")||(str=="delete")||(str=="union")||(str=="group")||(str=="having")) {
		res.send(200, {});
	   	data.date=new Date();
	   	log(data);
	   	return false;
	   }
	}
    }
 } 
};

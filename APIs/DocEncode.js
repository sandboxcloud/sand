var fs = require('fs');
var detectEncoding =require('detect-character-encoding');
var log = require('./log/log');
var data = {"name":"encode", "date" : ""};

module.exports = (file1, file2) =>{
 var content1 = fs.readFileSync(file1);
 var content2 = fs.readFileSync(file2);
 var enc1 = detectEncoding(content1);
 var enc2 = detectEncoding(content2);

 if(enc1 === enc2)
  return true;
 else{
  data.date = new Date();
  console.log(data);
  log(data);
  return false;
 }
}

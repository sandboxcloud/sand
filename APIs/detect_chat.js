//created by hjkang
//prevent from submitting same message and same IP
//num : number of same data, IP

//number of same data
import http from 'http';
import requestIP from 'request-ip';
var result = 0;

exports.DataDetect = function(contentMap, msg, num)		//contentMap : content가 들어있는 배열을 메인으로부터 받아옴, num : 사용자가 지정한 차단 횟수, msg : input message
{
	if(contentMap.hasOwnProperty(msg) === false)		//when the array is empty
		contentMap[msg] = 0;							//initialize array

	if(contentMap[msg] > num-1)
	{
		console.log('too many same data : ', contentMap.toString());
		result = 1;		//error case
	}
	else
	{
		console.log(contentMap.toString(), ":",  ++contentMap[msg]); //counting data
		result = 0;
	}

	return result;
}

exports.TrafficDetect = function(trafficMap, req, num)		//trafficMap : IP가 들어있는 배열을 메인으로부터 받아옴., req : server.js에서 받았던 request message를 인자로 넘겨줌
{
	var ClientIP = req.connection.remoteAddress;
	//return value ::xxxx:???.???.???.??? string form, so we have to slice string
    var SliceIndex = ClientIP.lastIndexOf(':');
    var ClientIP = ClientIP.substring(SliceIndex+1, ClientIP.length);

    if(trafficMap.hasOwnProperty(ClientIP) === false)	//ip에 해당하는 배열 속성이 없을 경우 초기화
    	trafficMap[ClientIP] = 0;

    //접근 횟수가 많으면 에러 메세지와 함께 1 리턴
	if(trafficMap[ClientIP] > num-1)
	{
		console.log('too many access! : ' + ClientIP);
		result = 1;
	}
	else
	{
		trafficMap[ClientIP]++;		//increase number of access
		console.log('Client IP : ' + ClientIP);
		console.log('number of Accesses : ' + trafficMap[ClientIP]);
		result = 0;
	}

	return result;
};

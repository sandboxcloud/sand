var fs = require('fs');
var log = require('./log/log');
var data = {"name": "size", "date":""};

module.exports = (file1, file2, num) => {
 var stat1 = fs.lstatSync(file1);
 var stat2 = fs.lstatSync(file2);

 var size1 = stat1.size;
 var size2 = stat2.size;

 if(num === 0){
  console.err("input error");
  return -1;
 }

 var diff = Math.abs(size1-size2);
 var cmp = diff-num;
 
 if(cmp>0){
  return true;
 }else{
  data.date = new Date();
  log(data);
  return false;
 }
}

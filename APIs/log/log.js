var fs = require('fs');
var path = require('path');

var log = (data, loca) => {
 var filename = 'log.json';

 if(loca == undefined) {
  loca='./';
 }

 var file = path.join(loca, filename);
 fs.appendFileSync(file, JSON.stringify(data)+'\n', function(err){
  if(err) throw err;
 });
}

module.exports = log;

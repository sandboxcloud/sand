var ipmap = new Map();
var log = require('./log/log');
var data = {"name" : "traffic", "date" : ""};

module.exports = (critical, limit) => {
  /*
  critical = ip와 매칭되는 클라이언트를 유해한 접속으로 판단할 접속횟수 기준
  limit = 접속횟수를 저장할 시간 단위 (s)
  */
  this.timer = setInterval(function(){
    //타이머를 설정하여 파라미터 limit에 맞게 접속횟수를 다시 초기화합니다.
    ipmap.clear();
  }, limit*1000);

  return function(req, res, next){
    var ClientIp = req.connection.remoteAddress;
    var SliceIndex = ClientIp.lastIndexOf(':');
    var ClientIp = ClientIp.substring(SliceIndex+1, ClientIp.length);

    if(ipmap.has(ClientIp) == false){
      ipmap.set(ClientIp, 1);
      next()
      return true;
    }else{
      ipmap.set(ClientIp, ipmap.get(ClientIp) + 1);
        if(ipmap.get(ClientIp) > critical){
          console.log('Warning : ' + ClientIp + '->' + 'Num of Access : ' + ipmap.get(ClientIp));
          data.date = new Date();
          log(data);
          next();
          return false;
        }else{
          next()
          return true;
        }
    }
    next();
  }
};

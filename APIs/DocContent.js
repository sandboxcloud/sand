var fs = require('fs');
var log = require('./log/log');
var data = {"name" : "Content", "date": ""};

module.exports = (file1, file2) => {
 var stat1 = fs.lstatSync(file1);
 var stat2 = fs.lstatSync(file2);

 var size1 = stat1.size;
 var size2 = stat2.size;

 var content1, content2;

 if(size1 === size2){
  content1 = fs.readFileSync(file1, 'utf8');
  content2 = fs.readFileSync(file2, 'utf8');

  if(content1 !== content2){
   data.date = new Date();
   log(data);
   return false;
  }
 }

 return true;
}

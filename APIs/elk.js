//elk connect
var exec=require('child_process').exec, 
	child;

module.exports = () => {
	//elasticsearch server 실행하는 명령어입니다
	child=exec("./elasticsearch-5.6.0/bin/elasticsearch", function(error, stdout, stderr) {
		console.log('stdout: '+ stdout);
		console.log('stderr: ' + stderr);
		if(error != null) {
			console.log('exec error: '+error);
		}
		return false;
	});

	//logstash.conf 실행하는 명령어입니다
	child=exec("./logstash-5.6.0/bin/logstash -f ./logstash-5.6.0/bin/logstash.conf", function(error, stdout, stderr) {
		console.log('stdout: '+ stdout);
		console.log('stderr: ' + stderr);
		if(error != null) {
			console.log('exec error: '+error);
		}
		return false;
	});

};

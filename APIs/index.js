var DocSize = require('./DocSize');
var DocEncode = require('./DocEncode');
var DocContent = require('./DocContent');
var shellDetec = require('./shell_injection');
var traffic = require('./traffic');
var sqlDetec=require('./sql_injection');
var elk=require('./elk');

exports.DocSize = DocSize;
exports.DocEncode = DocEncode;
exports.DocContent = DocContent;
exports.shellDetec = shellDetec;
exports.traffic = traffic;
exports.sqlDetec=sqlDetec;
exports.elk=elk;

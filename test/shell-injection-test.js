var secure = require('../APIs');
var fs = require('fs');

console.log("shell injection detect test");
var input = fs.createReadStream('./test-file/shell-test.txt');
input.on('data', function(chunk){
 var str = chunk.toString();
 var arr = str.split(",");
 for(var i=0; i<arr.length;i++){
   console.log("detect : " + arr[i]);
   console.log("result : " + secure.shellDetec(arr[i]));
 }
});

var secure = require('../APIs');
var file1 = './test-file/encode-en_US.UTF-8.txt';
var file2 = './test-file/encode-kr_KR.euckr.txt';
var encode = secure.DocEncode(file1, file2);

console.log("file1 is en_US.UTF-8");
console.log("file2 is kr_KR.euckr");
console.log("result : " + encode);

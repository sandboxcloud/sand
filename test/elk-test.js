var secure = require('./APIs');
var elk=require('elasticsearch'); //elasticsearch 모듈을 호출해주세요
var express = require('express');
var app=express();

//해당 모듈을 호출하여 서버 내에서 자동으로 명령어를 실행하도록 합니다
secure.elk();

var client=new elk.Client({
	host: 'localhost'
});

app.get('/', function(req, res){
  res.send('Hello World!');
})

app.listen(3000, function(){
  console.log('3000 port using...')
})

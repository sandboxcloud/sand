# *README*

Jinho-Park, Heejung-Kang, Joahong-Yun, Oheun-Kwon

## Anomaly Security node.js API

#### Node.js API로 제공되는 보안 솔루션

### 1.개요

개발자가 개발 단계에서 사용할 수 있는 Node.js  API로 제공되는 보안 솔루션 입니다.  

막고 싶은 행동이 있을 경우에 알맞은 API를 사용하고 API가 필요로 하는 인자를 넘깁니다.

API는 전달된 인자를 통해 anomaly를 판단하여 개발자에게 return 값으로 결과를 알려줍니다.

return의 형태는 console, log file, elk stack 등의 형태로 이루어 질 수 있습니다.

![프레젠테이션1.png](https://bitbucket.org/repo/koB8aM/images/2974096148-%ED%94%84%EB%A0%88%EC%A0%A0%ED%85%8C%EC%9D%B4%EC%85%981.png)

### 2.사용된 기술 및 사용 환경

- Node.js 7.10.0
- Ubuntu 16.04 LTS
- ElasticSearch 2.4.6, Kibana 4.5.4, Logstash 5.6.0

### 3.사용 방법 및 예시

전체적인 사용 방법은 프로젝트가 완성 된 다음 수정하도록 하겠습니다.



사용 예시는 다음과 같습니다.

```javascript
var secure = require('./APIs');
var DocContent = secure.DocContent(file1, file2);
var DocEncode = secure.DocEncode(file1, file2);
var DocSize = secure.DocSize(file1, file2, num); //num 두 파일의 크기 차이 제한
var ShellDetec = secure.ShellDetec(string);
var sqlDetec=secure.sqlDetec(str); //str은 사용자가 웹서버에 입력한 값을 갖는 매개변수
var elk=secure.elk(); //elastic과 바로 연결
var traffic = secure.traffic(critical, limit);
express().use(traffic);
```

require의 경우 APIs의 폴더를 include 해주시면 됩니다.

모든 모듈은 감지가 되었을 경우 false를 반환 감지가 없을 경우 true를 반환합니다.

로그 데이터는 현재 모듈을 사용한 파일의 디렉토리에 log.json으로 저장이 됩니다.

err값을 지정해 주어서 어느 부분에서 에러가 났는지 판단이 가능하게끔 할 수 있습니다.

결과 처리는 콘솔 창으로 보여주거나 로그 파일로 저장하여 [ELK stack](https://bitbucket.org/sandboxcloud/sand/wiki/ELK%20stack%20%EC%82%AC%EC%9A%A9%EB%B2%95) 등으로 활용할 수 있습니다.



### 4. test

전체적인 모듈의 테스트를 원하실 경우 아래의 command를 따라 입력하시면 됩니다.


```
$git clone
$npm install
$npm rebuild
$cd test
$node test.js
```
각 모듈에 대한 불할 테스르를 원하실 경우 test 폴더 내에 있는 js 파일들을 실행하시면 됩니다.  
다른 테스트 케이스를 넣고 싶으시다면 test폴더 내에 있는 test-files 폴더로 들어가셔서 txt 파일들을 수정하시면 됩니다.  